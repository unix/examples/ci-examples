# GitLab CI Examples

This repository contains examples of `.gitlab-ci.yml` configurations for
unit testing.

Please checkout this project's branches for configurations for various
languages and build tools:

- [`c-clang`](https://gitlab.fi.muni.cz/unix/ci-examples/tree/c-cmake/) (C)
    - CLang compiler suite
    - CMake build tool
    - [Catch](https://github.com/catchorg/Catch2) test framework
    - separate `build` and `test` stages
- [`java-maven`](https://gitlab.fi.muni.cz/unix/ci-examples/tree/java-maven/) (Java 8)
    - OpenJDK compiler
    - Maven build tool
    - JUnit test framework
- [`python-pipenv`](https://gitlab.fi.muni.cz/unix/ci-examples/tree/python-pipenv/) (Python)
    - Pipenv build tool
    - pytest test framework
- [`ruby-rake`](https://gitlab.fi.muni.cz/unix/ci-examples/tree/ruby-rake/) (Ruby)
    - Rake build tool
    - RSpec tests

You can help us expand these examples. Simply fork this repository,
create a branch from the initial tag and call it `<language>-<tool>`,
e.g. `haskell-cabal`:

```sh
git clone 'git@gitlab.fi.muni.cz:USER/ci-examples'
cd ci-examples
git checkout initial
git checkout -b haskell-cabal
```

Then prepare and test your `.gitlab-ci.yml` configuration with some simple
library. When finished, let us know you want to add a new branch.
After we confirm its creation, you can submit a merge request.
